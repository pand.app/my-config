{ ... }:
{
  homebrew = {
    enable = true;
    onActivation = {
      upgrade = true;
      autoUpdate = true;
      cleanup = "zap";
    };

    global = {
      autoUpdate = true;
      brewfile = true;
      lockfiles = true;
    };

    brews = [ ];

    casks = [
      # Base tools
      "keka" # To compress files
      "vlc"
      "firefox@developer-edition" # I use firefox as primary browser, to support plurality of browser engine, while everyone use webkit, and give power to Google.
      "thunderbird" # For emails, Mail app is pretty nice but thunderbird is more open, easier to backup, to open eml, 0 config to change for a privacy-ready reader...
      "ghostty"
      # Development
      "insomnia" # Pretty nice REST & GraphQL GUI
      "mactex-no-gui"
      "inkscape"
      "mysqlworkbench"
      "jetbrains-toolbox" # My favorite code editor suite (I've licences)
      "zed" # To open simple projects that don't require debugging tool
      ## Alternative Web Browser for development purpose
      "google-chrome@canary"
      "podman-desktop"

      # Productivity
      "rectangle-pro" # Window management without clic
      "logseq" # To take notes an organize knowledge
      "meetingbar" # Very useful app to see un menu bar what's my next meeting
      "numi" # A better calc app
      "1password"
      "maccy" # For clipboard management
      "jordanbaird-ice" # To custom menu bar
      "claude"
    ];

    masApps = {
      "Pomo timer" = 1447569061; # For high pressure days, split my day into ~30min, and focus on one task
      "Perplexity AI" = 6714467650;
    };
  };
}
