{
  config,
  lib,
  pkgs,
  ...
}:

{
  # Create directory for container-compose files
  system.activationScripts.createPersonalWebsiteDir = ''
    mkdir -p /etc/container-compose/personal-website
  '';

  # Create docker-compose file for personal website
  environment.etc."container-compose/personal-website/docker-compose.yml".text = ''
    version: '3'
    services:
      website:
        image: registry.gitlab.com/pand.app/gael-demette.fr/develop:latest
        container_name: personal-website
        restart: always
        pull_policy: always
        ports:
          - 127.0.0.1:3000:3000
  '';

  # Systemd service for personal website docker-compose
  systemd.services.personal-website-compose = {
    description = "Personal Website Docker Compose Service";
    wantedBy = [ "multi-user.target" ];
    requires = [ "podman.service" ];
    after = [
      "podman.service"
      "network.target"
    ];
    path = [
      pkgs.podman
      pkgs.podman-compose
    ];
    serviceConfig = {
      Type = "oneshot";
      RemainAfterExit = true;
      WorkingDirectory = "/etc/container-compose/personal-website";
      ExecStart = "${pkgs.podman-compose}/bin/podman-compose up -d";
      ExecStop = "${pkgs.podman-compose}/bin/podman-compose down";
    };
  };

  # Nginx virtual host configuration
  services.nginx.virtualHosts."gael-demette.fr" = {
    enableACME = true;
    forceSSL = true;
    locations."/" = {
      proxyPass = "http://localhost:3000";
      proxyWebsockets = true;
    };
  };
}