{
  ...
}:

{
  # Create directory for container-compose files
  system.activationScripts.container-compose-dir = ''
    mkdir -p /etc/container-compose
    chown -R panda:users /etc/container-compose
  '';
}
